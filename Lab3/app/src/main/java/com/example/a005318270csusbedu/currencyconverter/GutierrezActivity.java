package com.example.a005318270csusbedu.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GutierrezActivity extends AppCompatActivity {
    //Declare some variables
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;

    //String used to extract real-time currency exchange rates, using JSON, for the Petrol-AirCraftCarrier-Federal-Reserve-Note
    private static final String url = "https://api.fixer.io/latest?base=USD";
    //JSON Object extracted from website
    String json = "";
    String line;
    String rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gutierrez);
//cast the variables to their ids
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

//Click event
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                //obtain the latest currency-conversion value from URL, multiply it by the user input

                System.out.println("\nTESTING 1...before AsynchExecution\n");

                BackgroundTask object = new BackgroundTask();
                object.execute();

                System.out.println("\nTESTING 2... After AsynchExecution\n");

            }
        });
    }
    private class BackgroundTask extends AsyncTask<String, Void, String> {
        @Override
        //the method we use specifying what happens before the Asynchronous Task
        protected void onPreExecute() { super.onPreExecute();}

        @Override
        //the method we use specifying what happens during the progress of teh Asynchronous Task executing
        protected void onProgressUpdate(Void... values) { super.onProgressUpdate(values); }

        @Override
        //this is the method for the immediate aftermath of the Asynchronous Task execution
        protected void onPostExecute(String result) { super.onPostExecute(result);
            System.out.println("\nWhat is rate: " + rate + "\n");

            Double value = Double.parseDouble(rate);
            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsyncTask: " + value);
            //covert user's input to string

            usd = editText01.getText().toString();
            //if-else statement to make sure user cannot leave the EditText blank
            if (usd.equals("")) {
                textView01.setText("This field cannot be blank!");
            } else {
                //Convert string to double
                Double dInputs = Double.parseDouble(usd);
                //Convert function
                Double output = dInputs * value;
                //Display the result
                textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f", output));
                //clear the edit text after clicking
                editText01.setText("");
            }
        }
        //this method within the definition Background class will contain the vast Bulk of our code for the Asynchronous Task
        @Override
        protected String doInBackground(String... params) {
            try {
                //create an object from the URL Class and initialize it to the 'url' string in this Java Class, MainActivity
                URL web_url = new URL(GutierrezActivity.this.url);

                //create an object from the HTTPURLConnection class named httpURLConnection and initialize it with (HttpURLConnection)web_url.openConnection()
                //where the method openConnection() is a method defined in the URL class
                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();

                //Request method set as GET
                httpURLConnection.setRequestMethod("GET");

                System.out.println("\nTESTING...BEFORE connection method to URL\n");

                //invoke the connect() method from the object httpURLConnection
                httpURLConnection.connect();

                //create an object from the class InputStream and initialize object with
                InputStream inputStream = httpURLConnection.getInputStream();

                //create object named bufferedReader from the BufferedReader class and initialize the object with new BufferedReader(new InputStreamReader(inputStream)
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("\nCONNECTION SUCCESSFUL\n");

                //extract the string from the JSON, line by line, store it in the 'json' string variable, using a while lopp, checking until the end of the entire JSON string
                while (line != null) {
                    //We will assign the bufferedReader.readLine() to the string line every iteration
                    line = bufferedReader.readLine();
                    //then we will append the line to json
                    json += line;
                }
                System.out.println("\nTHE JSON :" + json);

                /**create JSON Object from JSONObject Class, using the json string**/
                JSONObject obj = new JSONObject(json);

                //create second JSON object that will contain a nested JSON Object within the FIRST JSON Object created
                JSONObject objRate = obj.getJSONObject("rates");

                //use the second JSON Object created and use the get(String ...) method
                //we will put "JPY" as the argument in the parameter of the get(String...) method in order to get the exchange rate for Yen
                //the logic code for currency conversion MUST GO INTO the onPostExecute() Method within AsynchTask
                rate = objRate.get("JPY").toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }
            return null;
        }
    }
}
